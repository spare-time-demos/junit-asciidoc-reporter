package org.manathome.adocreporter.writer;

public class WriteException extends RuntimeException {

  private static final long serialVersionUID = -6290584868161194228L;

  public WriteException() {
    super();
  }

  public WriteException(String message, Throwable cause) {
    super(message, cause);
  }

  public WriteException(String message) {
    super(message);
  }

}
