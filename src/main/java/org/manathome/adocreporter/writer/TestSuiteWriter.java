package org.manathome.adocreporter.writer;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Paths;

import org.manathome.adocreporter.assertions.Require;
import org.manathome.adocreporter.domain.TestCase;
import org.manathome.adocreporter.domain.TestCaseStatus;
import org.manathome.adocreporter.domain.TestSuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSuiteWriter {
  
  private static final Logger log = LoggerFactory.getLogger(TestSuiteWriter.class);

  private final String baseDirectory;
  private final TestSuite testSuite;

  public TestSuiteWriter(final String baseDirectory, final TestSuite testSuite) {
    this.baseDirectory = Require.notNullOrEmptyWhitespace(baseDirectory);
    this.testSuite = Require.notNull(testSuite, "testSuite");
  }
  
  /** write asciidoc file. */
  public void write() {

    try {
      try (PrintWriter writer = new PrintWriter(Paths.get(baseDirectory, getFilename()).toFile())) {
        log.trace("creating markup " + getFilename());
        writeContent(writer);
      }
    } catch (IOException ioe) {
      throw new WriteException("could not create " + getFilename() + " " + ioe.getMessage(), ioe);
    }
  }
  
  /** document one test case in detail. */
  public void writeContent(PrintWriter writer) {
    writer.println("= " + this.testSuite.getName());
    
    writeSummary(writer);
    
    writer.println();
    for (TestCase tc: testSuite.getTestcases()) {
      TestCaseWriter tcw = new TestCaseWriter(baseDirectory, tc);
      tcw.writeContent(writer);
    }
    writer.flush();
  }
  
  public String getFilename() {
    return testSuite.getName() + ".adoc";
  }
  
  private void writeSummary(PrintWriter writer) {
    
    writer.println();
    writer.println("|===");
    writer.println("|tests | passed | duration");
    writer.println();
    
    writer.println("| " + testSuite.getTestcases().size());
    writer.println("| " + testSuite.getTestcases()
                                   .stream()
                                   .filter(tc -> tc.getStatus() == TestCaseStatus.Passed)
                                   .count());
    writer.println("| " + testSuite.getTestcases()
                                  .stream()
                                  .map(tc -> tc.getDuration())
                                  .reduce(BigDecimal::add)
                                  .orElse(BigDecimal.ZERO));
    writer.println();
    writer.println("|===");    
  }
  
}
