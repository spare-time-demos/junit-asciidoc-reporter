package org.manathome.adocreporter.writer;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import org.manathome.adocreporter.assertions.Require;
import org.manathome.adocreporter.reader.TestCollector;
import org.manathome.adocreporter.reader.TestCollector.TestPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCollectorWriter {
  
  private static final Logger log = LoggerFactory.getLogger(TestCollectorWriter.class);

  private final String baseDirectory;
  private final TestCollector testCollector;

  public TestCollectorWriter(
      final String baseDirectory, 
      final TestCollector testCollector) {
    this.baseDirectory = Require.notNullOrEmptyWhitespace(baseDirectory);
    this.testCollector = Require.notNull(testCollector, "testCollector");
  }
  
  /** write asciidoc file. */
  public void write() {

    try {
      try (PrintWriter writer = new PrintWriter(Paths.get(baseDirectory, getFilename()).toFile())) {
        log.trace("creating markup " + getFilename());
        writeContent(writer);
      }
    } catch (IOException ioe) {
      throw new WriteException("could not create " + getFilename() + " " + ioe.getMessage(), ioe);
    }
  }
  
  /** document all collected tests. */
  public void writeContent(PrintWriter writer) {
    writer.println("= " + testCollector.getName());
    
    writer.println();
    writer.println("|===");
    writer.println("|test | result");
    writer.println();
    
    testCollector.getTests().forEachOrdered(tp -> {
      writer.println("| " + tp.testSuite.getName() + " " + tp.testCase.getFriendlyName());
      writer.println("| " + tp.testCase.getStatus());
      writer.println();
    });
    writer.println("|===");
    writer.flush();
  }
  
  public String getFilename() {
    return "tests.adoc";
  }
  
  protected String createMarkupLink(TestPair tp) {
    
    // TODO: link asciidoctor research!! not yet there.
    return "<<" 
        + tp.testSuite.getName() 
        + "#" + tp.testCase.getFqName() 
        + "," 
        + tp.testSuite.getName() + " " + tp.testCase.getFriendlyName() 
        + ">>";
  }
  
}
