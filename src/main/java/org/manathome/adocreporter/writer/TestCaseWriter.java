package org.manathome.adocreporter.writer;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import org.manathome.adocreporter.assertions.Require;
import org.manathome.adocreporter.domain.TestCase;
import org.manathome.adocreporter.domain.TestCaseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCaseWriter {

  private static final Logger log = LoggerFactory.getLogger(TestCaseWriter.class);

  private final String   baseDirectory;
  private final TestCase testCase;
  
  public TestCaseWriter(final String baseDirectory, final TestCase testCase) {
    this.baseDirectory = Require.notNullOrEmptyWhitespace(baseDirectory);
    this.testCase = Require.notNull(testCase, "testCase");
  }

  /** write asciidoc file. */
  public void write() {

    try {
      try (PrintWriter writer = new PrintWriter(Paths.get(baseDirectory, getFilename()).toFile())) {
        log.trace("creating markup " + getFilename());
        writeContent(writer);
      }
    } catch (IOException ioe) {
      throw new WriteException("could not create " + getFilename() + " " + ioe.getMessage(), ioe);
    }
  }

  /** document one test case in detail. */
  public void writeContent(PrintWriter writer) {

    writer.println("== " 
        + this.testCase.getFriendlyName() 
        + "[[" + sanitizeAsIdRef(this.testCase.getFqName()) + "]]");
    writer.println();

    if (testCase.getStatus() == TestCaseStatus.Errored 
        || testCase.getStatus() == TestCaseStatus.Failed) {
      writer.println("[WARNING]"); 
      writer.println("." + testCase.getStatus());
      writer.println("====");
      writer.println(testCase.getUnsuccessfulMessage());
      writer.println("====");
      writer.println();
    }
    
    if (testCase.getStatus() == TestCaseStatus.Skipped) {
      writer.println("NOTE: " + testCase.getStatus());
      writer.println();
    }

    
    if (testCase.getDstlLines().size() > 0) {
      writer.println("=== spec "); 
      testCase.getDstlLines().forEach(writer::println);
      writer.println();       
    }

    writer.println("=== result "); 

    if (testCase.getStackTrace() != null) {
      writer.println(".stacktrace");
      writer.println("----");
      writer.println("." + testCase.getUnsuccessfulMessage());
      writer.println("----");
      writer.println();
    }

    if (testCase.getSystemOut() != null && testCase.getSystemOut().size() > 0) {
      writer.println(".console stdout");
      writer.println("----");
      testCase.getSystemOut().forEach(l -> writer.println(" " + l));
      writer.println("----");
      writer.println();
    }

    if (testCase.getSystemErr() != null && testCase.getSystemErr().size() > 0) {
      writer.println(".console stderr");
      writer.println("----");
      testCase.getSystemErr().forEach(l -> writer.println(" " + l));
      writer.println("----");
      writer.println();
    }
    
    writer.flush();
  }

  private String sanitizeAsIdRef(final String name) {
    
    if (name == null) {
      return "";
    }
    
    return name
        .replace(")", "")
        .replace("(", ""); 
  }

  public String getFilename() {
    return testCase.getName() + ".adoc";
  }

}
