package org.manathome.adocreporter.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.manathome.adocreporter.domain.TestCase;
import org.manathome.adocreporter.domain.TestSuite;

/** collect test cases from multiple test suites. */
public class TestCollector {

  private final List<TestPair> testCases = new ArrayList<TestPair>();
  private final String name;
  
  public TestCollector(final String name) {
    this.name = name;
  }
  
  /** add matching test cases from test suite. */
  public boolean collectOnMatch(TestSuite testSuite) {
    
    testSuite
      .getTestcases()
      .stream()
      .map(tc -> new TestPair(testSuite, tc))
      .forEach(this.testCases::add);

    return true;
  }
  
  public Stream<TestPair> getTests() {
    return this.testCases.stream();
  }

  public String getName() {
    return name;
  }

  public class TestPair {
    
    public TestPair(final TestSuite testSuite, final TestCase testCase) {
      this.testCase = testCase;
      this.testSuite = testSuite;
    }
    
    public TestSuite testSuite;
    public TestCase testCase;
  }

}
