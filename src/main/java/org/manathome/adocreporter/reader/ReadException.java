package org.manathome.adocreporter.reader;

public class ReadException extends RuntimeException {

  private static final long serialVersionUID = -6290584838161194228L;

  public ReadException() {
    super();
  }

  public ReadException(String message, Throwable cause) {
    super(message, cause);
  }

  public ReadException(String message) {
    super(message);
  }

}
