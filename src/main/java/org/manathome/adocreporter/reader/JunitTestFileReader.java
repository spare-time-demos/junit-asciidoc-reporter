package org.manathome.adocreporter.reader;

import static org.manathome.adocreporter.domain.TestCaseStatus.Errored;
import static org.manathome.adocreporter.domain.TestCaseStatus.Failed;
import static org.manathome.adocreporter.domain.TestCaseStatus.Passed;
import static org.manathome.adocreporter.domain.TestCaseStatus.Skipped;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.manathome.adocreporter.domain.TestCase;
import org.manathome.adocreporter.domain.TestCaseStatus;
import org.manathome.adocreporter.domain.TestSuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/** read from xml file. */
public class JunitTestFileReader {

  private static final Logger log = LoggerFactory.getLogger(JunitTestFileReader.class);
  
  private TestSuite testSuite = new TestSuite(); 

  /** .ctor. */
  public JunitTestFileReader(
      final String testSuiteName, 
      final InputStream in)  throws ParserConfigurationException, SAXException, IOException {

    this.testSuite.setName(testSuiteName);
    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(in);

    NamedNodeMap testsuiteAttrs = doc.getDocumentElement().getAttributes();
    if (testsuiteAttrs != null) {
      for (int i = 0; i < testsuiteAttrs.getLength(); i++) {
        final Node attr = testsuiteAttrs.item(i);
        final String name = attr.getNodeName();
        final String val = attr.getNodeValue();
        if (name.equalsIgnoreCase("name")) {
          testSuite.setName(val);
        }
        testSuite.getSystemProperties().put(name, val);
      }
    }

    NodeList nl = doc.getElementsByTagName("property");
    for (int i = 0; i < nl.getLength(); i++) {
      Node node = nl.item(i);
      testSuite.getSystemProperties().put(attr(node, "name"), attr(node, "value"));
    }

    testSuite.getSysOut().addAll(handleTextNode(child(doc.getDocumentElement(), "system-out")));
    testSuite.getSysErr().addAll(handleTextNode(child(doc.getDocumentElement(), "system-err")));

    nl = doc.getElementsByTagName("testcase");
    for (int i = 0; i < nl.getLength(); i++) {
      Node node = nl.item(i);

      TestCaseStatus status = Passed; // default output is success.

      String unsuccessfulMessage = null;
      String stackTrace = null;

      Node statusNode = child(node, "skipped");
      if (statusNode != null) {
        status = Skipped;
        unsuccessfulMessage = attr(statusNode, "message");
        stackTrace = statusNode.getTextContent();
      }

      statusNode = child(node, "failure");
      if (statusNode != null) {
        status = Failed;
        unsuccessfulMessage = attr(statusNode, "message");
        stackTrace = statusNode.getTextContent();
      }

      statusNode = child(node, "error");
      if (statusNode != null) {
        status = Errored;

        String message = attr(statusNode, "message");
        String type = attr(statusNode, "type");
        if (message != null && type != null) {
          unsuccessfulMessage = type + ": " + message;
        } else if (type != null) {
          unsuccessfulMessage = type;
        }
        stackTrace = statusNode.getTextContent();
      }

      final TestCase testCase = new TestCase(
          attr(node, "name"), 
          attr(node, "classname"), 
          new BigDecimal(attr(node, "time")),
          status);

      testCase.setUnsuccessfulMessage(unsuccessfulMessage);
      testCase.setStackTrace(stackTrace);
      testCase.getSystemOut().addAll(handleTextNode(child(node, "system-out")));
      testCase.getSystemErr().addAll(handleTextNode(child(node, "system-err")));
      testSuite.getTestcases().add(testCase);
      
      log.debug("added " + testSuite.getTestcases().size() 
          + " results to test suite " + testSuiteName);
    }

  }

  private String attr(Node node, String name) {
    Node item = node.getAttributes().getNamedItem(name);
    return item == null ? null : item.getTextContent();
  }

  private Node child(Node parent, String name) {
    NodeList nl = parent.getChildNodes();
    for (int i = 0; i < nl.getLength(); i++) {
      Node child = nl.item(i);
      if (name.equals(child.getNodeName())) {
        return child;
      }
    }
    return null;
  }

  private List<String> handleTextNode(Node node) throws IOException {
    List<String> lines = new ArrayList<>();
    if (node != null) {
      NodeList nl = node.getChildNodes();
      for (int i = 0; i < nl.getLength(); i++) {
        Node child = nl.item(i);

        // switch to BufferedReader rather than string split to handle unix/windows
        // cross platform rendering
        final BufferedReader bIn = new BufferedReader(new StringReader(child.getTextContent()));
        String line = bIn.readLine();
        while (line != null) {
          lines.add(line);
          line = bIn.readLine();
        }

      }
    }
    return lines;
  }

  public TestSuite getTestSuite() {
    return this.testSuite;
  }

}
