package org.manathome.adocreporter.gradle;

/** gradle configuration properties for reporter task. */
public class JunitAsciidocReporterExtension {
  
  public static final String settingsExtensionName = "junitAsciidocReport";

  private String testResultsDir = "build/test-results";
  private String testReportsDir = "build/reports/tests";


  public void setTestReportsDir(final String testReportsDir) {
    this.testReportsDir = testReportsDir;
  }

  public String getTestReportsDir() {
    return testReportsDir;
  }

  public void setTestResultsDir(final String testResultsDir) {
    this.testResultsDir = testResultsDir;
  }

  public String getTestResultsDir() {
    return testResultsDir;
  }

}
