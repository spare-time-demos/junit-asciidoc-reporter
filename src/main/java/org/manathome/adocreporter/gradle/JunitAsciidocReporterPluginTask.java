package org.manathome.adocreporter.gradle;

import java.io.File;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.manathome.adocreporter.ReportGenerator;
import org.manathome.adocreporter.reader.ReadException;
import org.manathome.adocreporter.writer.WriteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JunitAsciidocReporterPluginTask extends DefaultTask {

  private static final Logger log = LoggerFactory.getLogger(JunitAsciidocReporterPluginTask.class);

  public static final String taskName = "asciidocReporter";

  /** ctor. */
  public JunitAsciidocReporterPluginTask() {
    super();
    setGroup("documentation");
    setDescription("Parses the JUnit5 produced test xml and produces a simple html report");
  }

  /** . */
  @TaskAction
  public void generateJunitReport() throws Exception {

    log.info("Starting generateJunitReport() task");
    System.out.println("generating asciidoc test report");

    try {

      JunitAsciidocReporterExtension settings = (JunitAsciidocReporterExtension) getProject()
          .getExtensions()
          .getByName(JunitAsciidocReporterExtension.settingsExtensionName);

      if (settings == null) {
        System.out.println("no settings");
        settings = new JunitAsciidocReporterExtension(); // use defaults
      }

      File buildDir = getProject().getBuildDir();
      log.debug("buildDir = " + buildDir.getAbsolutePath());

      if (settings.getTestResultsDir() == null) {
        throw new WriteException("config for testResultsDir is missing");
      }
      if (settings.getTestReportsDir() == null) {
        throw new ReadException("config for testReportsDir is missing");
      }

      log.debug("testResultsDir = " + buildDir.getAbsolutePath());

      ReportGenerator processor = new ReportGenerator();

      int suiteCount = processor.generate(
          settings.getTestResultsDir(),
          settings.getTestReportsDir() 
          );

      log.debug("found " + suiteCount + " test suites run results");

    } catch (Exception ex) {
      System.out.println("aciidocReporter: error " + ex.getMessage());
      throw ex;
    }
  }

}
