package org.manathome.adocreporter.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** use reporter as gradle build plugin. */
public class JunitAsciidocReporterPlugin implements Plugin<Project> {

  private final Logger log = LoggerFactory.getLogger(this.getClass());
  
  @Override
  public void apply(Project project) {
    
    System.out.println("apply project");
    
    log.debug("asciidoc reporter plugins");
    
    project.getTasks().register(
        JunitAsciidocReporterPluginTask.taskName, 
        JunitAsciidocReporterPluginTask.class);

    project.getExtensions().create(
        JunitAsciidocReporterExtension.settingsExtensionName,
        JunitAsciidocReporterExtension.class);
        
    // addFinalizedBy("test", project, task);
    // addFinalizedBy("check", project, task);
  }

  protected void addFinalizedBy(
      String taskName, 
      Project project, 
      JunitAsciidocReporterPluginTask reporterTask) {
    
    project.getTasks().forEach(task -> {
      if (plainTaskName(task.getName()).equals(taskName)) {
        task.finalizedBy(reporterTask);
      }
    });
    
  }

  /** . */
  public String plainTaskName(String taskName) {
    if (taskName.contains(":")) {
      taskName = taskName.substring(taskName.lastIndexOf(":") + 1);
    }
    return taskName;
  }
  

}

