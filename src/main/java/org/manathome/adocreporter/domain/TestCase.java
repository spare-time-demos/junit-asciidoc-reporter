package org.manathome.adocreporter.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.manathome.adocreporter.dstl.Dstl;

public class TestCase {

  private final String         name;     // test name;
  private final String         classname;
  private final BigDecimal     duration; // time
  private final TestCaseStatus status;
  private final List<String>   systemOut;
  private final List<String>   systemErr;

  private String unsuccessfulMessage;
  private String stackTrace;

  /** ctor. */
  public TestCase(String name, String classname, BigDecimal duration, TestCaseStatus status) {
    this.name = name;
    this.classname = classname;
    this.duration = duration == null ? BigDecimal.ZERO : duration;
    this.status = status;
    systemOut = new ArrayList<>();
    systemErr = new ArrayList<>();
  }

  public String getName() {
    return name;
  }
  
  /** prose name. */
  public String getFriendlyName() {
    return name
        .replace("_", " ")
        .replace("()", "");        
  }
  
  /** prose name. */
  public String getFqName() {
    return classname + "." + name;
  }

  public String getClassname() {
    return classname;
  }

  /** package part of classname. */
  public String getPackagename() {
    if (classname.contains(".")) {
      return classname.substring(0, classname.lastIndexOf("."));
    }
    return ""; // no package!!
  }

  public BigDecimal getDuration() {
    return duration;
  }

  public TestCaseStatus getStatus() {
    return status;
  }

  public List<String> getSystemOut() {
    return systemOut;
  }
  
  /** get dead simple test log lines from systemOut. */
  public List<String> getDstlLines() {
    return systemOut
              .stream()
              .filter(Dstl::isDslLine)
              .collect(Collectors.toList())
              ;
  }

  public List<String> getSystemErr() {
    return systemErr;
  }

  public void setUnsuccessfulMessage(String unsuccessfulMessage) {
    this.unsuccessfulMessage = unsuccessfulMessage;
  }

  public String getUnsuccessfulMessage() {
    return unsuccessfulMessage;
  }

  public void setStackTrace(String stackTrace) {
    this.stackTrace = stackTrace;
  }

  public String getStackTrace() {
    return stackTrace;
  }
}
