package org.manathome.adocreporter.domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.manathome.adocreporter.assertions.Require;

public class TestSuite {
  
  private String name;
  
  private final LinkedHashMap<String, String> systemProperties    = new LinkedHashMap<>();
  private final LinkedHashMap<String, String> testsuiteProperties = new LinkedHashMap<>();
  private final List<TestCase>                testcases           = new ArrayList<>();
  private final List<String>                  sysOut              = new ArrayList<>();
  private final List<String>                  sysErr              = new ArrayList<>();
  

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = Require.notNullOrEmptyWhitespace(name);    
  }

  public LinkedHashMap<String, String> getSystemProperties() {
    return systemProperties;
  }

  public LinkedHashMap<String, String> getTestsuiteProperties() {
    return testsuiteProperties;
  }

  public List<TestCase> getTestcases() {
    return testcases;
  }

  public List<String> getSysErr() {
    return sysErr;
  }
  
  public List<String> getSysOut() {
    return sysOut;
  }
  
}
