package org.manathome.adocreporter.domain;

public enum TestCaseStatus {
  Passed, 
  Skipped, 
  Failed, 
  Errored;
}
