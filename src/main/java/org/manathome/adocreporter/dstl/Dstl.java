package org.manathome.adocreporter.dstl;

/** produce "dead simple test lines" as integrated in junit-asciidoc-generator output. */
public class Dstl {
  
  public static final String PRAEFIX = "DSTL";

  /** document test fixture / start condition. */
  public static void given(String precondition) {
    System.out.println(PRAEFIX + "-GIVEN..:" + precondition);
  }
  
  /** document test action. */
  public static void when(String action) {
    System.out.println(PRAEFIX + "-WHEN...:" + action);
  }

  /** document test result. */
  public static void then(String result) {
    System.out.println(PRAEFIX + "-THEN...:" + result);
  }

  /** document test action details. */
  public static void log(String logTestDetails) {
    System.out.println(PRAEFIX + "-LOG....:" + logTestDetails);
  }
  
  /** counts as a valid dslt formatted line. */
  public static boolean isDslLine(String line) {
    return line != null 
        && line.startsWith(PRAEFIX + "-");
  }
  
  /** emit default asciidoc markup line. */
  public static String toMarkup(String line) {

    if (line == null || line.isEmpty()) {
      return "";
    }

    if (isDslLine(line)) {
      
      String s = line.substring(PRAEFIX.length());
      
      s = s.replace("-GIVEN..:", "*Given* ");
      s = s.replace("-WHEN...:", "*When* ");
      s = s.replace("-THEN...:", "*Then* ");
      s = s.replace("-LOG....:", "");
      
      return s;
      
    } else {
      return "                      _" + line + "_";   // not a valid line deemphasize
    }
  }

}
