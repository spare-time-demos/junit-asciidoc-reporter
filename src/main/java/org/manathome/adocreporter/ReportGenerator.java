package org.manathome.adocreporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.manathome.adocreporter.assertions.Require;
import org.manathome.adocreporter.domain.TestSuite;
import org.manathome.adocreporter.reader.JunitTestFileReader;
import org.manathome.adocreporter.reader.ReadException;
import org.manathome.adocreporter.reader.TestCollector;
import org.manathome.adocreporter.writer.TestCollectorWriter;
import org.manathome.adocreporter.writer.TestSuiteWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportGenerator {

  private static final Logger log = LoggerFactory.getLogger(ReportGenerator.class);

  public ReportGenerator() {
  }

  /** generate asciidoc markup for test output. */
  public int generate(String inputBasePath, String outputBasePath) throws Exception {

    log.info("test markup from " 
        + Require.notNullOrEmptyWhitespace(inputBasePath, "inputBasePath") 
        + " into " + Require.notNullOrEmptyWhitespace(outputBasePath, "outputBasePath"));

    Require.isTrue(Files.exists(Paths.get(inputBasePath)), 
        "inputBasePath not found: " 
         + inputBasePath + " " 
         + Paths.get(inputBasePath).toAbsolutePath());

    Require.isTrue(Files.isDirectory(Paths.get(inputBasePath)), 
                   "inputBasePath not a Directory: " + inputBasePath);
    Require.isTrue(Files.isDirectory(Paths.get(outputBasePath)), 
                   "outputBasePath not a Directory " + outputBasePath);
    
    String[] xmlFiles = scanFiles(inputBasePath);

    if (xmlFiles.length > 0) {
      
      final List<TestSuite> suites = new ArrayList<>();
      final TestCollector collectorAll = new TestCollector("all tests");
      
      // read
      for (String fileName : xmlFiles) {
        try (InputStream in = new FileInputStream(new File(fileName))) {
          
          final JunitTestFileReader reader = new JunitTestFileReader(
              Paths.get(fileName).getFileName().toString(), 
              in);
          
          suites.add(reader.getTestSuite());
          collectorAll.collectOnMatch(reader.getTestSuite());
          
        }
      }
      
      // write
      suites.forEach(s -> new TestSuiteWriter(outputBasePath, s).write());
      new TestCollectorWriter(outputBasePath, collectorAll).write();
      
      log.info("test markup done.");
      
      return suites.size();
    }
    return 0;
  }

  /** find all files. */
  public String[] scanFiles(final String basePath) {
    try (Stream<Path> paths = Files.walk(Paths.get(basePath))) {

      return paths
          .filter(Files::isRegularFile)
          .filter(p -> p.toString().toLowerCase().endsWith(".xml"))
          .filter(p -> p.getFileName().toString().toLowerCase().startsWith("test-"))
          .map(p -> p.toAbsolutePath().toString()).toArray(String[]::new);

    } catch (IOException e) {
      throw new ReadException(e.getMessage(), e);
    }
  }

}
