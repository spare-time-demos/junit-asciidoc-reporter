package org.manathome.adocreporter.assertions;

public class RequirementNotMet extends AssertionError {

  private static final long serialVersionUID = 1L;
  
  public RequirementNotMet(final String msg) {
    super(msg);
  }

}
