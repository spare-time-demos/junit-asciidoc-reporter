package org.manathome.adocreporter.assertions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** custom assertions to express preconditions or postconditions in code. */
public final class Require {

  static final Logger logger = LoggerFactory.getLogger(Require.class);

  private Require() {
  }

  /** check not null. 
   * @param notNullObject object to assert
   * @param nameOfNotNullExpression name the assertions.
  */
  public static <T> T notNull(final T notNullObject, final String nameOfNotNullExpression) {
    if (notNullObject == null) {
      logger.error("requirement not null violated on: " + nameOfNotNullExpression);
      throw new RequirementNotMet("requirement not null violated on: " + nameOfNotNullExpression);
    }
    return notNullObject;
  }

  /** check not null. 
   * @param notNullObject object to assert
   * */
  public static <T> T notNull(final T notNullObject) {
    return notNull(notNullObject, "object not null expected");
  }

  /** allow neither null nor blank or empty string. 
   * @param text string to assert
   */
  public static String notNullOrEmptyWhitespace(final String text) {
    return notNullOrEmptyWhitespace(text, "non empty text required");
  }

  /** allow neither null nor blank or empty string. */
  public static String notNullOrEmptyWhitespace(final String text, final String errorMsg) {
    String t = notNull(text, errorMsg);
    if (t.trim().length() == 0) {
      logger.error(errorMsg);
      throw new RequirementNotMet(errorMsg);
    }
    return t;
  }

  /** require positive or 0,  >= 1 numbers. 
   * 
   * @param number to assert
   * */
  public static int zeroOrPositive(int number) {
    if (number < 0) {
      logger.error("zeroOrPositive: value is negative: " + number);
      throw new RequirementNotMet("value is negative: " + number);
    }
    return number;
  }

  /** test to be true.
   * 
   * @param shouldBeTrue assert this
   * @return true
   */
  public static boolean isTrue(boolean shouldBeTrue) {
    return isTrue(shouldBeTrue, "required check not true");
  }

  /** ensure true.
   * @param shouldBeTrue condition to assert
   * @param msg assert failed message
   * @return true
   */
  public static boolean isTrue(boolean shouldBeTrue, String msg) {

    if (!shouldBeTrue) {
      throw new RequirementNotMet(msg);
    }
    return true;
  }

  /** ensure number not 0 or null.
   * 
   * @param number to assert
   * @return number
   */
  public static Long notNullOrZero(Long number) {
    return notNullOrZero(number, null);
  }

  /** ensure number is given and not 0 or null.
   * @param number to assert
   * @param name name of number value.
   * @return number 
   */
  public static Long notNullOrZero(final Long number, final String name) {

    if (Require.notNull(number).longValue() == 0) {
      throw new RequirementNotMet("number shall not be zero (0) or null " 
                    + (name == null ? "." : "for: " + name));
    }
    return number;
  }

}
