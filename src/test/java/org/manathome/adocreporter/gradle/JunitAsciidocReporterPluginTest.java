package org.manathome.adocreporter.gradle;

import static org.assertj.core.api.Assertions.assertThat;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.Test;

/** basic check as gradle plugin. */
public class JunitAsciidocReporterPluginTest {

  @Test 
  public void pluginRegistersATask() {
    // Create a test project and apply the plugin
    Project project = ProjectBuilder.builder().build();
    project.getPlugins().apply("org.manathome.adocreporter");

    // Verify the result
    assertThat(project.getTasks().findByName("asciidocReport"));
  }
}
