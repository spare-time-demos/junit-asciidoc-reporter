package org.manathome.adocreporter;

import static org.assertj.core.api.Assertions.assertThat;

import static org.manathome.adocreporter.dstl.Dstl.given;
import static org.manathome.adocreporter.dstl.Dstl.then;
import static org.manathome.adocreporter.dstl.Dstl.when;

import java.io.File;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.manathome.adocreporter.dstl.Dstl;


@DisplayName("ReportGeneratorTest: basic input output processing test xml and markdown output")
public class ReportGeneratorTest {
  
  private ReportGenerator generator = new ReportGenerator();

  @Test
  void scan_test_files_in_directory() {
    
    given("a directory with test xml files TEST-*.xml");
    
    File file = new File(getClass()
        .getClassLoader()
        .getResource("test.input.xml/TEST-org.manathome.timeatwork.domain.ProjectTest.xml")
        .getFile());
    assertThat(file).as("test file found").isNotNull();
    
    when("scanning");
    
    String[] fileNames = generator.scanFiles(file.getParent());

    then("all files should be found");

    assertThat(fileNames.length)
      .as("unexpected Test Files in directory test.input.xml")
      .isEqualTo(8);
  }

  @Test
  void generate_Markup_From_XmlFiles() throws Exception {

    given("a full generator run");

    File file = new File(getClass()
        .getClassLoader()
        .getResource("test.input.xml/TEST-org.manathome.timeatwork.domain.ProjectTest.xml")
        .getFile());

    Dstl.log("path to search: " + file.getParent());
    
    then("produces markdown output (TBD)");
    
    generator.generate(file.getParent(), file.getParent());
  }

}
