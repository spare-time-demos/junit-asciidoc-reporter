package org.manathome.adocreporter.writer;

import static org.assertj.core.api.Assertions.assertThat;

import static org.manathome.adocreporter.dstl.Dstl.given;
import static org.manathome.adocreporter.dstl.Dstl.then;
import static org.manathome.adocreporter.dstl.Dstl.when;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.manathome.adocreporter.domain.TestCase;
import org.manathome.adocreporter.domain.TestCaseStatus;

public class TestCaseWriterTest {
  
  private TestCase testCase;
  
  /** test fixture. */
  @BeforeEach
  public void beforeEach() {
    this.testCase = new TestCase(
        "valid_test_case_Name()", 
        "MyTestClassName", 
        BigDecimal.ZERO, 
        TestCaseStatus.Passed);
  }

  @Test
  public void markup_contains_test_name_header() {
    
    given("a testcase " + testCase.getName());
    
    when("production markup");
    StringWriter sw = new StringWriter();
    TestCaseWriter writer = new TestCaseWriter(".", testCase);
    writer.writeContent(new PrintWriter(sw));
    
    then("markup contains header with testcase name");
    assertThat(sw.toString()).contains("= valid test case Name");
  }

}
