package org.manathome.adocreporter.writer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.manathome.adocreporter.reader.JunitTestFileReader;
import org.manathome.adocreporter.reader.TestCollector;

public class TestCollectorWriterTest {

  private static TestCollector testCollector;

  /** test fixture. */
  @BeforeAll
  public static void beforeAll() throws Exception {

    testCollector = new TestCollector("my collector");

    InputStream in = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream(
            "test.input.xml/TEST-org.manathome.timeatwork.domain.ProjectTest.xml");

    assertThat(testCollector
        .collectOnMatch(new JunitTestFileReader("junit-test1", in).getTestSuite()))
      .isTrue();

    in = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream(
            "test.input.xml/TEST-org.manathome.timeatwork.domain.TaskAssignmentTest.xml");

    assertThat(testCollector
        .collectOnMatch(new JunitTestFileReader("junit-test2", in).getTestSuite()))
      .isTrue();
  }

  @Test
  public void markup_shows_header() {
    StringWriter sw = new StringWriter();
    TestCollectorWriter writer = new TestCollectorWriter("my header", testCollector);
    writer.writeContent(new PrintWriter(sw));
    final String markup = sw.toString();
    
    assertThat(markup).contains("= my collector");
  }

  @Test
  public void markup_shows_cases_from_multiple_suites() {
    StringWriter sw = new StringWriter();
    TestCollectorWriter writer = new TestCollectorWriter("my header", testCollector);
    writer.writeContent(new PrintWriter(sw));
    final String markup = sw.toString();
    
    assertThat(markup).contains("createProject");
    assertThat(markup).contains("testTaskAssignmentAllowesWork");
  }

}
