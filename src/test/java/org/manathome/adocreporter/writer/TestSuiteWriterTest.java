package org.manathome.adocreporter.writer;

import static org.assertj.core.api.Assertions.assertThat;

import static org.manathome.adocreporter.dstl.Dstl.given;
import static org.manathome.adocreporter.dstl.Dstl.then;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.manathome.adocreporter.domain.TestSuite;
import org.manathome.adocreporter.dstl.Dstl;
import org.manathome.adocreporter.reader.JunitTestFileReader;

public class TestSuiteWriterTest {

  private static TestSuite testSuite;
  private static String    markup;

  /** test fixture. */
  @BeforeAll
  public static void beforeEach() throws Exception {

    if (markup == null) {
      final InputStream in = Thread.currentThread()
          .getContextClassLoader()
          .getResourceAsStream(
              "test.input.xml/TEST-org.manathome.timeatwork.domain.ProjectTest.xml");
      
      testSuite = new JunitTestFileReader("junit-test", in).getTestSuite();

      StringWriter sw = new StringWriter();
      TestSuiteWriter writer = new TestSuiteWriter(".", testSuite);
      writer.writeContent(new PrintWriter(sw));
      markup = sw.toString();
      
      Dstl.log(markup);
    }
  }

  @Test
  public void markup_contains_suite_name_header() {
    assertThat(markup).contains("= org.manathome.timeatwork.domain.ProjectTest");
  }

  @Test
  public void markup_contains_all_test_case_header() {

    given("Project Testcase");
    then("Headers for each TestCase is generated.");

    assertThat(markup).contains("== testProjectRemoveUnknownLead");
    assertThat(markup).contains("== testProjectWithDuplicateLeads");
    assertThat(markup).contains("== createProjectWithLeads");
    assertThat(markup).contains("== createProject");
  }

}
