package org.manathome.adocreporter.reader;

import static org.assertj.core.api.Assertions.assertThat;

import static org.manathome.adocreporter.dstl.Dstl.given;
import static org.manathome.adocreporter.dstl.Dstl.then;
import static org.manathome.adocreporter.dstl.Dstl.when;

import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.manathome.adocreporter.domain.TestSuite;
import org.manathome.adocreporter.reader.JunitTestFileReader;

public class JunitTestFileReaderTest {

  private JunitTestFileReader readTestXmlReport() throws Exception {
    final InputStream in = Thread.currentThread()
        .getContextClassLoader()
        .getResourceAsStream(
            "test.input.xml/TEST-org.manathome.adocreporter.ReportGeneratorTest.xml");
    return new JunitTestFileReader("junit-test", in);
  }
 
  @Test
  public void can_read_junit_xmlFile() throws Exception {
    
    given("a simple xml testsuite");

    when(" parsing");
    final TestSuite ts = readTestXmlReport().getTestSuite();    
    assertThat(ts).isNotNull();
    
    then("suite name and testcases should be found");    
    assertThat(ts.getName()).contains("ReportGeneratorTest");
    assertThat(ts.getTestcases().size())
      .as("at least 2 test cases")
      .isGreaterThan(1);
  }

}
